package managers;

import triskeloperationtree.FunctionNode;

public abstract class DependencyManager {

    public abstract void excecute(FunctionNode functionNode);

}
