package managers.dependenciesManagers;

import Analyzers.object.ObjectAnalyzer;
import cubetriplestore.CubeTripleStoreQuery;
import java.util.List;
import managers.DependencyManager;
import managers.priorityManagers.NodePriorityManager;
import Analyzers.subject.SubjectAnalyzer;
import triskeloperationtree.FunctionNode;
import triskeloperationtree.Node;

public class NodeDependencyManager extends DependencyManager {

    private final NodePriorityManager nodePriorityManager;
    private final SubjectAnalyzer subjectAnalyzer;
    private final ObjectAnalyzer objectAnalyzer;

    public NodeDependencyManager(CubeTripleStoreQuery cubeTripleStoreQuery) {
        this.nodePriorityManager = new NodePriorityManager();
        this.subjectAnalyzer = new SubjectAnalyzer(cubeTripleStoreQuery);
        this.objectAnalyzer = new ObjectAnalyzer(cubeTripleStoreQuery);
    }

    @Override
    public void excecute(FunctionNode functionNode) {
        List<Node> orderOneNodeList = nodePriorityManager.getAllNodesOfOrder(1, functionNode);
        List<Node> orderTwoNodeList = nodePriorityManager.getAllNodesOfOrder(2, functionNode);
        if (orderTwoNodeList.isEmpty() || orderOneNodeList.isEmpty()) {
            return;
        }
        subjectAnalyzer.checkSubjectDependencies(orderOneNodeList, orderTwoNodeList, functionNode);
        objectAnalyzer.checkObjectDependencies(orderOneNodeList, orderTwoNodeList, functionNode);
    }
}
