package managers.priorityManagers;

import java.util.ArrayList;
import java.util.List;
import managers.PriorityManager;
import triskeloperationtree.FunctionNode;
import triskeloperationtree.Node;
import triskeloperationtree.PatternNode;

public class NodePriorityManager extends PriorityManager {

    @Override
    public void excecute(FunctionNode functionNode) {
        List<Node> sortedList = getSortedNodeList(functionNode);
        for (int i = 0; i < functionNode.getChildNumber(); i++) {
            functionNode.changeNode(i, sortedList.get(i));
        }
    }

    private List<Node> getSortedNodeList(FunctionNode functionNode) {
        List<Node> sortedList = new ArrayList();
        for (int i = 0; i <= 3; i++) {
            sortedList.addAll(getAllNodesOfOrder(i, functionNode));
        }
        sortedList.addAll(getAllOtherFunctionNodes(functionNode));
        return sortedList;
    }

    public List<Node> getAllNodesOfOrder(int order, FunctionNode functionNode) {
        List<Node> list = new ArrayList();
        for (int i = 0; i < functionNode.getChildNumber(); i++) {
            if (functionNode.getNode(i) instanceof PatternNode) {
                PatternNode patternNode = (PatternNode) functionNode.getNode(i);
                if (patternNode.getPattern().getOrder() == order) {
                    list.add(patternNode);
                }
            }
        }
        return list;
    }

    private List<Node> getAllOtherFunctionNodes(FunctionNode functionNode) {
        List<Node> list = new ArrayList();
        for (int i = 0; i < functionNode.getChildNumber(); i++) {
            if (functionNode.getNode(i) instanceof FunctionNode) {
                list.add(functionNode.getNode(i));
            }
        }
        return list;
    }
}
