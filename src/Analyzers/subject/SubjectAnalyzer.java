package Analyzers.subject;

import Analyzers.Analyzer;
import cubetriplestore.CubeTripleStoreQuery;
import java.util.List;
import patternmatching.PatternQuery;
import patterns.Constant;
import patterns.Pattern;
import patterns.What;
import triskelTable.Table;
import triskeloperationtree.FunctionNode;
import triskeloperationtree.Node;
import triskeloperationtree.PatternNode;
import triskeloperationtree.TableNode;

public class SubjectAnalyzer extends Analyzer {

    private final CubeTripleStoreQuery cubeTripleStoreQuery;

    public SubjectAnalyzer(CubeTripleStoreQuery cubeTripleStoreQuery) {
        this.cubeTripleStoreQuery = cubeTripleStoreQuery;
    }

    public void checkSubjectDependencies(List<Node> orderOneNodeList, List<Node> orderTwoNodeList, FunctionNode functionNode) {
        for (int orderOneIndex = 0; orderOneIndex < orderOneNodeList.size(); orderOneIndex++) {
            PatternNode patternNodeOrderOne = (PatternNode) orderOneNodeList.get(orderOneIndex);
            String whatNameOrderOne = getSubjectWhatName(patternNodeOrderOne);
            for (int orderTwoIndex = 0; orderTwoIndex < orderTwoNodeList.size(); orderTwoIndex++) {
                if (functionNode.getNode(orderOneNodeList.size() + orderTwoIndex) instanceof TableNode) {
                    continue;
                }
                checkSubjectSubjectDependency(orderTwoNodeList, orderTwoIndex, whatNameOrderOne, patternNodeOrderOne, functionNode, orderOneIndex, orderOneNodeList);
            }
        }
    }

    private String getSubjectWhatName(PatternNode patternNode) {
        if (patternNode.getPattern().isItSubjectWhat()) {
            What SubjectWhat = (What) patternNode.getPattern().getSubject();
            return SubjectWhat.getName();
        }
        return "";
    }

    private void checkSubjectSubjectDependency(List<Node> orderTwoNodeList, int orderTwoIndex, String whatNameOrderOne, PatternNode patternNodeOrderOne, FunctionNode functionNode, int orderOneIndex, List<Node> orderOneNodeList) {
        PatternNode patternNodeOrderTwo = (PatternNode) orderTwoNodeList.get(orderTwoIndex);
        String whatNameOrderTwo = getSubjectWhatName(patternNodeOrderTwo);
        if (whatNameOrderOne.equals(whatNameOrderTwo)) {
            Table table = executePatternNode(patternNodeOrderOne).getTable();
            functionNode.changeNode(orderOneIndex, new TableNode(table));
            long[] patternIndexes = getTableWhatIDs(table, whatNameOrderOne);
            Table tableResult = excecuteDependency(patternIndexes, patternNodeOrderTwo, whatNameOrderOne);
            functionNode.changeNode(orderOneNodeList.size() + orderTwoIndex, new TableNode(tableResult));
        }
    }

    private Table excecuteDependency(long[] patternIndexes, PatternNode patternNodeOrderTwo, String whatNameOrderOne) {
        Table table = null;
        Table tableResult = null;
        for (int index = 0; index < patternIndexes.length; index++) {
            table = executePattern(patternIndexes, index, patternNodeOrderTwo);
            tableResult = createNewTableResult(tableResult, whatNameOrderOne, table);
            updateTableResult(table, tableResult, patternIndexes, index);
        }
        return tableResult;
    }

    private void updateTableResult(Table table, Table tableResult, long[] patternIndexes, int index) {
        for (int rowIndex = 0; rowIndex < table.size(); rowIndex++) {
            tableResult.addRow(new Long[]{patternIndexes[index], table.get(rowIndex, 0)});
        }
    }

    private Table createNewTableResult(Table tableResult, String whatNameOrderOne, Table table) {
        if (tableResult == null) {
            tableResult = new Table(new String[]{whatNameOrderOne, table.getColumns()[0]});
        }
        return tableResult;
    }

    private long[] getTableWhatIDs(Table table, String whatNameOrderOne) {
        long patternIndexes[] = new long[table.size()];
        if (table.size() != 0) {
            for (int k = 0; k < table.size(); k++) {
                patternIndexes[k] = table.get(k, whatNameOrderOne);
            }
        }
        return patternIndexes;
    }

    private Table executePattern(long[] patternIndexes, int l, PatternNode patternNodeOrderTwo) {
        Pattern dependencyPattern = new Pattern(new Constant(patternIndexes[l]), patternNodeOrderTwo.getPattern().getPredicate(), patternNodeOrderTwo.getPattern().getObject());
        PatternQuery patternQuery = cubeTripleStoreQuery.createPatternQuery(dependencyPattern);
        patternQuery.execute();
        return patternQuery.getTable();
    }

    private PatternQuery executePatternNode(PatternNode patternNodeOrderOne) {
        PatternQuery patternQuery = cubeTripleStoreQuery.createPatternQuery(patternNodeOrderOne.getPattern());
        patternQuery.execute();
        return patternQuery;
    }

    @Deprecated
    private void checkSubjectObjectDependency(List<Node> orderTwoNodeList, int orderTwoIndex, String whatNameOrderOne, PatternNode patternNodeOrderOne, FunctionNode functionNode, int orderOneIndex, List<Node> orderOneNodeList) {
        What objectWhat;
        String whatNameOrderTwo = "";
        PatternNode patternNodeOrderTwo = (PatternNode) orderTwoNodeList.get(orderTwoIndex);
        if (patternNodeOrderTwo.getPattern().isItObjectWhat()) {
            objectWhat = (What) patternNodeOrderTwo.getPattern().getObject();
            whatNameOrderTwo = objectWhat.getName();
        }
        if (whatNameOrderOne.equals(whatNameOrderTwo)) {
            PatternQuery patternQuery = executePatternNode(patternNodeOrderOne);
            Table table = patternQuery.getTable();
            functionNode.changeNode(orderOneIndex, new TableNode(patternQuery.getTable()));
            long[] patternIndexes = getTableWhatIDs(table, whatNameOrderOne);
            Table tableResult = null;
            for (int l = 0; l < patternIndexes.length; l++) {
                Pattern dependencyPattern = new Pattern(patternNodeOrderTwo.getPattern().getSubject(), patternNodeOrderTwo.getPattern().getPredicate(), new Constant(patternIndexes[l]));
                patternQuery = cubeTripleStoreQuery.createPatternQuery(dependencyPattern);
                patternQuery.execute();
                table = patternQuery.getTable();
                tableResult = createNewTableResult(tableResult, whatNameOrderOne, table);
                updateTableResult(table, tableResult, patternIndexes, l);
            }
            functionNode.changeNode(orderOneNodeList.size() + orderTwoIndex, new TableNode(tableResult));
        }
    }
}
