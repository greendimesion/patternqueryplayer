package Analyzers.object;

import Analyzers.Analyzer;
import cubetriplestore.CubeTripleStoreQuery;
import java.util.List;
import patternmatching.PatternQuery;
import patterns.Constant;
import patterns.Pattern;
import patterns.What;
import triskelTable.Table;
import triskeloperationtree.FunctionNode;
import triskeloperationtree.Node;
import triskeloperationtree.PatternNode;
import triskeloperationtree.TableNode;

public class ObjectAnalyzer extends Analyzer {

    private final CubeTripleStoreQuery cubeTripleStoreQuery;

    public ObjectAnalyzer(CubeTripleStoreQuery cubeTripleStoreQuery) {
        this.cubeTripleStoreQuery = cubeTripleStoreQuery;
    }

    public void checkObjectDependencies(List<Node> orderOneNodeList, List<Node> orderTwoNodeList, FunctionNode functionNode) {
        for (int orderOneIndex = 0; orderOneIndex < orderOneNodeList.size(); orderOneIndex++) {
            if (functionNode.getNode(orderOneIndex) instanceof TableNode) {
                continue;
            }
            PatternNode patternNodeOrderOne = (PatternNode) orderOneNodeList.get(orderOneIndex);
            String whatNameOrderOne = getWhatObjectName(patternNodeOrderOne);
            for (int orderTwoIndex = 0; orderTwoIndex < orderTwoNodeList.size(); orderTwoIndex++) {
                if (functionNode.getNode(orderTwoIndex) instanceof TableNode) {
                    continue;
                }
                checkobjectObjectDependency(orderTwoNodeList, orderTwoIndex, whatNameOrderOne, patternNodeOrderOne, functionNode, orderOneIndex, orderOneNodeList);
            }

        }
    }

    private String getWhatObjectName(PatternNode patternNode) {
        if (patternNode.getPattern().isItObjectWhat()) {
            What ObjectWhat = (What) patternNode.getPattern().getObject();
            return ObjectWhat.getName();
        }
        return "";
    }

    private void updateTableResult(Table table, Table tableResult, long[] patternIndexes, int index) {
        for (int rowIndex = 0; rowIndex < table.size(); rowIndex++) {
            tableResult.addRow(new Long[]{patternIndexes[index], table.get(rowIndex, 0)});
        }
    }

    private Table createNewTableResult(Table tableResult, String whatNameOrderOne, Table table) {
        if (tableResult == null) {
            tableResult = new Table(new String[]{whatNameOrderOne, table.getColumns()[0]});
        }
        return tableResult;
    }

    private long[] getTableIDs(Table table, String whatNameOrderOne) {
        long patternIndexes[] = new long[table.size()];
        if (table.size() != 0) {
            for (int k = 0; k < table.size(); k++) {
                patternIndexes[k] = table.get(k, whatNameOrderOne);
            }
        }
        return patternIndexes;
    }

    private PatternQuery executePatternNode(PatternNode patternNode) {
        PatternQuery patternQuery = cubeTripleStoreQuery.createPatternQuery(patternNode.getPattern());
        patternQuery.execute();
        return patternQuery;
    }

    private void checkobjectObjectDependency(List<Node> orderTwoNodeList, int orderTwoIndex, String whatNameOrderOne, PatternNode patternNodeOrderOne, FunctionNode functionNode, int orderOneIndex, List<Node> orderOneNodeList) {
        PatternNode patternNodeOrderTwo = (PatternNode) orderTwoNodeList.get(orderTwoIndex);
        String whatNameOrderTwo = getWhatObjectName(patternNodeOrderTwo);
        if (whatNameOrderOne.equals(whatNameOrderTwo)) {
            Table table = executePatternNode(patternNodeOrderOne).getTable();
            functionNode.changeNode(orderOneIndex, new TableNode(table));
            long[] patternIndexes = getTableIDs(table, whatNameOrderOne);
            Table tableResult = excecuteDependency(patternIndexes, patternNodeOrderTwo, whatNameOrderOne);
            functionNode.changeNode(orderOneNodeList.size() + orderTwoIndex, new TableNode(tableResult));
        }
    }

    private Table excecuteDependency(long[] patternIndexes, PatternNode patternNodeOrderTwo, String whatNameOrderOne) {
        Table table;
        Table tableResult = null;
        for (int l = 0; l < patternIndexes.length; l++) {
            table = executePattern(patternNodeOrderTwo, patternIndexes, l);
            tableResult = createNewTableResult(tableResult, whatNameOrderOne, table);
            updateTableResult(table, tableResult, patternIndexes, l);
        }
        return tableResult;
    }

    private Table executePattern(PatternNode patternNodeOrderTwo, long[] patternIndexes, int l) {
        Pattern dependencyPattern = new Pattern(patternNodeOrderTwo.getPattern().getSubject(), patternNodeOrderTwo.getPattern().getPredicate(), new Constant(patternIndexes[l]));
        PatternQuery patternQuery = cubeTripleStoreQuery.createPatternQuery(dependencyPattern);
        patternQuery.execute();
        return patternQuery.getTable();
    }

    // Expensive Search.
    @Deprecated
    private void checkobjectSubjectDependency(List<Node> orderTwoNodeList, int orderTwoIndex, String whatNameOrderOne, PatternNode patternNodeOrderOne, FunctionNode functionNode, int orderOneIndex, List<Node> orderOneNodeList) {
        What objectWhat;
        String whatNameOrderTwo = "";
        PatternNode patternNodeOrderTwo = (PatternNode) orderTwoNodeList.get(orderTwoIndex);
        if (patternNodeOrderTwo.getPattern().isItSubjectWhat()) {
            objectWhat = (What) patternNodeOrderTwo.getPattern().getSubject();
            whatNameOrderTwo = objectWhat.getName();
        }
        if (whatNameOrderOne.equals(whatNameOrderTwo)) {
            PatternQuery patternQuery = executePatternNode(patternNodeOrderOne);
            Table table = patternQuery.getTable();
            functionNode.changeNode(orderOneIndex, new TableNode(patternQuery.getTable()));
            long[] patternIndexes = getTableIDs(table, whatNameOrderOne);
            Table tableResult = null;
            for (int l = 0; l < patternIndexes.length; l++) {
                Pattern dependencyPattern = new Pattern(new Constant(patternIndexes[l]), patternNodeOrderTwo.getPattern().getPredicate(), patternNodeOrderTwo.getPattern().getObject());
                patternQuery = cubeTripleStoreQuery.createPatternQuery(dependencyPattern);
                patternQuery.execute();
                table = patternQuery.getTable();
                tableResult = createNewTableResult(tableResult, whatNameOrderOne, table);
                updateTableResult(table, tableResult, patternIndexes, l);
            }
            functionNode.changeNode(orderOneNodeList.size() + orderTwoIndex, new TableNode(tableResult));
        }
    }

}
