package player.players;

import cubetriplestore.CubeTripleStoreQuery;
import java.util.ArrayList;
import java.util.List;
import patternmatching.PatternQuery;
import patterns.Constant;
import patterns.Pattern;
import patterns.What;
import player.Player;
import triskelTable.Table;
import triskeloperationtree.FunctionNode;
import triskeloperationtree.Node;
import triskeloperationtree.OperationTree;
import triskeloperationtree.PatternNode;
import triskeloperationtree.TableNode;

public class PriorityDependenciesPatternQueryPlayer extends Player {

    private final CubeTripleStoreQuery cubeTripleStoreQuery;
    private final OperationTree operationTree;

    public PriorityDependenciesPatternQueryPlayer(CubeTripleStoreQuery cubeTripleStoreQuery, OperationTree operationTree) {
        this.cubeTripleStoreQuery = cubeTripleStoreQuery;
        this.operationTree = operationTree;
    }

    @Override
    public Table excecute() {

        if (operationTree.getRoot() instanceof FunctionNode) {
            FunctionNode functionNode = (FunctionNode) operationTree.getRoot();
            return solvePattern(functionNode);
        }
        return null;
    }

    private Table solvePattern(FunctionNode functionNode) {
        sortNode(functionNode);
        analyseFullDendencies(functionNode);
        for (int i = 0; i < functionNode.getChildNumber(); i++) {
            if (functionNode.getNode(i) instanceof PatternNode) {
                PatternNode patternNode = (PatternNode) functionNode.getNode(i);
                PatternQuery patternQuery = cubeTripleStoreQuery.createPatternQuery(patternNode.getPattern());
                patternQuery.execute();
                functionNode.changeNode(i, new TableNode(patternQuery.getTable()));
            } else if (functionNode.getNode(i) instanceof FunctionNode) {
                System.out.println("An other function node");
                functionNode.changeNode(i, new TableNode(solvePattern((FunctionNode) functionNode.getNode(i))));
            }
        }
        return functionNode.execute();
    }

    public void sortNode(FunctionNode functionNode) {
        List<Node> sortedList = getSortedNodeList(functionNode);
        for (int i = 0; i < functionNode.getChildNumber(); i++) {
            functionNode.changeNode(i, sortedList.get(i));
        }
    }

    private List<Node> getSortedNodeList(FunctionNode functionNode) {
        List<Node> sortedList = new ArrayList();
        for (int i = 0; i <= 3; i++) {
            sortedList.addAll(getAllNodesOfOrder(i, functionNode));
        }
        sortedList.addAll(getAllOtherFunctionNodes(functionNode));
        return sortedList;
    }

    private List<Node> getAllNodesOfOrder(int order, FunctionNode functionNode) {
        List<Node> list = new ArrayList();
        for (int i = 0; i < functionNode.getChildNumber(); i++) {
            if (functionNode.getNode(i) instanceof PatternNode) {
                PatternNode patternNode = (PatternNode) functionNode.getNode(i);
                if (patternNode.getPattern().getOrder() == order) {
                    list.add(patternNode);
                }
            }
        }
        return list;
    }

    private List<Node> getAllOtherFunctionNodes(FunctionNode functionNode) {
        List<Node> list = new ArrayList();
        for (int i = 0; i < functionNode.getChildNumber(); i++) {
            if (functionNode.getNode(i) instanceof FunctionNode) {
                list.add(functionNode.getNode(i));
            }
        }
        return list;
    }

    private void analyseFullDendencies(FunctionNode functionNode) {
        List<Node> orderOneNodeList = getAllNodesOfOrder(1, functionNode);
        List<Node> orderTwoNodeList = getAllNodesOfOrder(2, functionNode);
        if (orderTwoNodeList.isEmpty() || orderOneNodeList.isEmpty()) {
            return;
        }
        checkSubjectDependencies(orderOneNodeList, orderTwoNodeList, functionNode);
        checkObjectDependencies(orderOneNodeList, orderTwoNodeList, functionNode);
    }

    private void checkSubjectDependencies(List<Node> orderOneNodeList, List<Node> orderTwoNodeList, FunctionNode functionNode) {
        for (int orderOneIndex = 0; orderOneIndex < orderOneNodeList.size(); orderOneIndex++) {
            PatternNode patternNodeOrderOne = (PatternNode) orderOneNodeList.get(orderOneIndex);
            What firstSubjectWhat = null;
            String whatNameOrderOne = "";
            if (patternNodeOrderOne.getPattern().isItSubjectWhat()) {
                firstSubjectWhat = (What) patternNodeOrderOne.getPattern().getSubject();
                whatNameOrderOne = firstSubjectWhat.getName();
            }
            for (int orderTwoIndex = 0; orderTwoIndex < orderTwoNodeList.size(); orderTwoIndex++) {
                if (functionNode.getNode(orderOneNodeList.size() + orderTwoIndex) instanceof TableNode) {
                    continue;
                }
                checkSubjectSubjectDependency(orderTwoNodeList, orderTwoIndex, whatNameOrderOne, patternNodeOrderOne, functionNode, orderOneIndex, orderOneNodeList);
            }

        }
    }

    private void checkObjectDependencies(List<Node> orderOneNodeList, List<Node> orderTwoNodeList, FunctionNode functionNode) {
        for (int orderOneIndex = 0; orderOneIndex < orderOneNodeList.size(); orderOneIndex++) {
            if (functionNode.getNode(orderOneIndex) instanceof TableNode) {
                continue;
            }
            PatternNode patternNodeOrderOne = (PatternNode) orderOneNodeList.get(orderOneIndex);
            What firstObjectWhat = null;
            String whatNameOrderOne = "";
            if (patternNodeOrderOne.getPattern().isItObjectWhat()) {
                firstObjectWhat = (What) patternNodeOrderOne.getPattern().getObject();
                whatNameOrderOne = firstObjectWhat.getName();
            }
            for (int orderTwoIndex = 0; orderTwoIndex < orderTwoNodeList.size(); orderTwoIndex++) {
                if (functionNode.getNode(orderTwoIndex) instanceof TableNode) {
                    continue;
                }
                checkobjectObjectDependency(orderTwoNodeList, orderTwoIndex, whatNameOrderOne, patternNodeOrderOne, functionNode, orderOneIndex, orderOneNodeList);
            }

        }
    }

    private void checkSubjectSubjectDependency(List<Node> orderTwoNodeList, int orderTwoIndex, String whatNameOrderOne, PatternNode patternNodeOrderOne, FunctionNode functionNode, int orderOneIndex, List<Node> orderOneNodeList) {
        What secondSubjectWhat;
        String whatNameOrderTwo = "";
        PatternNode patternNodeOrderTwo = (PatternNode) orderTwoNodeList.get(orderTwoIndex);
        if (patternNodeOrderTwo.getPattern().isItSubjectWhat()) {
            secondSubjectWhat = (What) patternNodeOrderTwo.getPattern().getSubject();
            whatNameOrderTwo = secondSubjectWhat.getName();
        }
        if (whatNameOrderOne.equals(whatNameOrderTwo)) {
            PatternQuery patternQuery = cubeTripleStoreQuery.createPatternQuery(patternNodeOrderOne.getPattern());
            patternQuery.execute();
            Table table = patternQuery.getTable();
            functionNode.changeNode(orderOneIndex, new TableNode(patternQuery.getTable()));
            long patternIndexes[] = new long[table.size()];
            if (table.size() != 0) {
                for (int k = 0; k < table.size(); k++) {
                    patternIndexes[k] = table.get(k, whatNameOrderOne);
                }
            }
            Table tableResult = null;
            for (int l = 0; l < patternIndexes.length; l++) {
                Pattern dependencyPattern = new Pattern(new Constant(patternIndexes[l]), patternNodeOrderTwo.getPattern().getPredicate(), patternNodeOrderTwo.getPattern().getObject());
                patternQuery = cubeTripleStoreQuery.createPatternQuery(dependencyPattern);
                patternQuery.execute();
                table = patternQuery.getTable();
                if (tableResult == null) {
                    tableResult = new Table(new String[]{whatNameOrderOne, table.getColumns()[0]});
                }
                for (int rowIndex = 0; rowIndex < table.size(); rowIndex++) {
                    tableResult.addRow(new Long[]{patternIndexes[l], table.get(rowIndex, 0)});
                }
            }
            functionNode.changeNode(orderOneNodeList.size() + orderTwoIndex, new TableNode(tableResult));
        }
    }

    private void checkobjectObjectDependency(List<Node> orderTwoNodeList, int orderTwoIndex, String whatNameOrderOne, PatternNode patternNodeOrderOne, FunctionNode functionNode, int orderOneIndex, List<Node> orderOneNodeList) {
        What secondObjectWhat;
        String whatNameOrderTwo = "";
        PatternNode patternNodeOrderTwo = (PatternNode) orderTwoNodeList.get(orderTwoIndex);
        if (patternNodeOrderTwo.getPattern().isItObjectWhat()) {
            secondObjectWhat = (What) patternNodeOrderTwo.getPattern().getObject();
            whatNameOrderTwo = secondObjectWhat.getName();
        }
        if (whatNameOrderOne.equals(whatNameOrderTwo)) {
            PatternQuery patternQuery = cubeTripleStoreQuery.createPatternQuery(patternNodeOrderOne.getPattern());
            patternQuery.execute();
            Table table = patternQuery.getTable();
            functionNode.changeNode(orderOneIndex, new TableNode(patternQuery.getTable()));
            long patternIndexes[] = new long[table.size()];
            if (table.size() != 0) {
                for (int k = 0; k < table.size(); k++) {
                    patternIndexes[k] = table.get(k, whatNameOrderOne);
                }
            }
            Table tableResult = null;
            for (int l = 0; l < patternIndexes.length; l++) {
                Pattern dependencyPattern = new Pattern(patternNodeOrderTwo.getPattern().getSubject(), patternNodeOrderTwo.getPattern().getPredicate(), new Constant(patternIndexes[l]));
                patternQuery = cubeTripleStoreQuery.createPatternQuery(dependencyPattern);
                patternQuery.execute();
                table = patternQuery.getTable();
                if (tableResult == null) {
                    tableResult = new Table(new String[]{whatNameOrderOne, table.getColumns()[0]});
                }
                for (int rowIndex = 0; rowIndex < table.size(); rowIndex++) {
                    tableResult.addRow(new Long[]{patternIndexes[l], table.get(rowIndex, 0)});
                }
            }
            functionNode.changeNode(orderOneNodeList.size() + orderTwoIndex, new TableNode(tableResult));
        }
    }

    @Deprecated
    private void checkSubjectObjectDependency(List<Node> orderTwoNodeList, int orderTwoIndex, String whatNameOrderOne, PatternNode patternNodeOrderOne, FunctionNode functionNode, int orderOneIndex, List<Node> orderOneNodeList) {
        What objectWhat;
        String whatNameOrderTwo = "";
        PatternNode patternNodeOrderTwo = (PatternNode) orderTwoNodeList.get(orderTwoIndex);
        if (patternNodeOrderTwo.getPattern().isItObjectWhat()) {
            objectWhat = (What) patternNodeOrderTwo.getPattern().getObject();
            whatNameOrderTwo = objectWhat.getName();
        }
        if (whatNameOrderOne.equals(whatNameOrderTwo)) {
            PatternQuery patternQuery = cubeTripleStoreQuery.createPatternQuery(patternNodeOrderOne.getPattern());
            patternQuery.execute();
            Table table = patternQuery.getTable();
            functionNode.changeNode(orderOneIndex, new TableNode(patternQuery.getTable()));
            long patternIndexes[] = new long[table.size()];
            if (table.size() != 0) {
                for (int k = 0; k < table.size(); k++) {
                    patternIndexes[k] = table.get(k, whatNameOrderOne);
                }
            }
            Table tableResult = null;
            for (int l = 0; l < patternIndexes.length; l++) {
                Pattern dependencyPattern = new Pattern(patternNodeOrderTwo.getPattern().getSubject(), patternNodeOrderTwo.getPattern().getPredicate(), new Constant(patternIndexes[l]));
                patternQuery = cubeTripleStoreQuery.createPatternQuery(dependencyPattern);
                patternQuery.execute();
                table = patternQuery.getTable();
                if (tableResult == null) {
                    tableResult = new Table(new String[]{whatNameOrderOne, table.getColumns()[0]});
                }
                for (int rowIndex = 0; rowIndex < table.size(); rowIndex++) {
                    tableResult.addRow(new Long[]{patternIndexes[l], table.get(rowIndex, 0)});
                }
            }
            functionNode.changeNode(orderOneNodeList.size() + orderTwoIndex, new TableNode(tableResult));
        }
    }

    // Expensive Search.
    @Deprecated
    private void checkobjectSubjectDependency(List<Node> orderTwoNodeList, int orderTwoIndex, String whatNameOrderOne, PatternNode patternNodeOrderOne, FunctionNode functionNode, int orderOneIndex, List<Node> orderOneNodeList) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
