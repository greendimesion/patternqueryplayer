package player.players;

import cubetriplestore.CubeTripleStoreQuery;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import patternmatching.PatternQuery;
import patterns.Constant;
import patterns.Pattern;
import patterns.What;
import player.Player;
import triskelTable.Table;
import triskeloperationtree.FunctionNode;
import triskeloperationtree.Node;
import triskeloperationtree.OperationTree;
import triskeloperationtree.PatternNode;
import triskeloperationtree.TableNode;

public class MultiLevelDependencyPlayer extends Player {

    private final CubeTripleStoreQuery cubeTripleStoreQuery;
    private final OperationTree operationTree;
    private HashMap<String, Table> subjectDependencyBoard;
    private HashMap<String, Table> objectDependencyBoard;

    public MultiLevelDependencyPlayer(CubeTripleStoreQuery cubeTripleStoreQuery, OperationTree operationTree) {
        this.cubeTripleStoreQuery = cubeTripleStoreQuery;
        this.operationTree = operationTree;
        this.subjectDependencyBoard = new HashMap<>();
        this.objectDependencyBoard = new HashMap<>();
    }

    @Override
    public Table excecute() {

        if (operationTree.getRoot() instanceof FunctionNode) {
            FunctionNode functionNode = (FunctionNode) operationTree.getRoot();
            return solvePattern(functionNode);
        }
        return null;
    }

    private Table solvePattern(FunctionNode functionNode) {
        sortNode(functionNode);
        for (int i = 0; i < functionNode.getChildNumber(); i++) {
            if (functionNode.getNode(i) instanceof PatternNode) {
                PatternNode patternNode = (PatternNode) functionNode.getNode(i);
                if (patternNode.getPattern().getOrder() == 1) {
                    executeGradeOnePatterns(patternNode, functionNode, i);
                } else {
                    if (patternNode.getPattern().getOrder() == 2) {
                        checkSubjectDependency(patternNode, functionNode, i);
                        checkObjectDependency(functionNode, i, patternNode);
                    }
                }
            } else if (functionNode.getNode(i) instanceof FunctionNode) {
                functionNode.changeNode(i, new TableNode(solvePattern((FunctionNode) functionNode.getNode(i))));
            }
        }
        return functionNode.execute();
    }

    private void checkObjectDependency(FunctionNode functionNode, int i, PatternNode patternNode) {
        if (functionNode.getNode(i) instanceof PatternNode) {
            if (patternNode.getPattern().isItObjectWhat()) {
                What what = (What) patternNode.getPattern().getObject();
                String whatName = what.getName();
                Table table = this.objectDependencyBoard.get(whatName);
                if (table != null) {
                    long patternIndexes[] = new long[table.size()];
                    if (table.size() != 0) {
                        for (int k = 0; k < table.size(); k++) {
                            patternIndexes[k] = table.get(k, whatName);
                        }
                    }
                    Table tableResult = null;
                    for (int l = 0; l < patternIndexes.length; l++) {
                        Pattern dependencyPattern = new Pattern(patternNode.getPattern().getSubject(), patternNode.getPattern().getPredicate(), new Constant(patternIndexes[l]));
                        PatternQuery patternQuery = cubeTripleStoreQuery.createPatternQuery(dependencyPattern);
                        patternQuery.execute();
                        table = patternQuery.getTable();
                        if (tableResult == null) {
                            tableResult = new Table(new String[]{table.getColumns()[0], whatName});
                        }
                        for (int rowIndex = 0; rowIndex < table.size(); rowIndex++) {
                            tableResult.addRow(new Long[]{table.get(rowIndex, 0), patternIndexes[l],});
                        }
                    }
                    functionNode.changeNode(i, new TableNode(tableResult));
                } else {
                    PatternQuery patternQuery = executePattern(patternNode);
                    functionNode.changeNode(i, new TableNode(patternQuery.getTable()));
                }
            }
        }
    }

    private void checkSubjectDependency(PatternNode patternNode, FunctionNode functionNode, int i) {
        if (patternNode.getPattern().isItSubjectWhat()) {
            What what = (What) patternNode.getPattern().getSubject();
            String whatName = what.getName();
            Table table = this.subjectDependencyBoard.get(whatName);
            if (table != null) {
                long patternIndexes[] = new long[table.size()];
                if (table.size() != 0) {
                    for (int k = 0; k < table.size(); k++) {
                        patternIndexes[k] = table.get(k, whatName);
                    }
                }
                Table tableResult = null;
                for (int l = 0; l < patternIndexes.length; l++) {
                    Pattern dependencyPattern = new Pattern(new Constant(patternIndexes[l]), patternNode.getPattern().getPredicate(), patternNode.getPattern().getObject());
                    PatternQuery patternQuery = cubeTripleStoreQuery.createPatternQuery(dependencyPattern);
                    patternQuery.execute();
                    table = patternQuery.getTable();
                    if (tableResult == null) {
                        tableResult = new Table(new String[]{whatName, table.getColumns()[0]});
                    }
                    for (int rowIndex = 0; rowIndex < table.size(); rowIndex++) {
                        tableResult.addRow(new Long[]{patternIndexes[l], table.get(rowIndex, 0)});
                    }
                }
                functionNode.changeNode(i, new TableNode(tableResult));
            } else {
                PatternQuery patternQuery = executePattern(patternNode);
                functionNode.changeNode(i, new TableNode(patternQuery.getTable()));
            }
        }
    }

    private PatternQuery executePattern(PatternNode patternNode) {
        PatternQuery patternQuery = cubeTripleStoreQuery.createPatternQuery(patternNode.getPattern());
        patternQuery.execute();
        return patternQuery;
    }

    private void executeGradeOnePatterns(PatternNode patternNode, FunctionNode functionNode, int i) {
        PatternQuery patternQuery = executePattern(patternNode);
        functionNode.changeNode(i, new TableNode(patternQuery.getTable()));
        excecuteSubject(patternNode, patternQuery);
        executeObject(patternNode, patternQuery);
    }

    private void executeObject(PatternNode patternNode, PatternQuery patternQuery) {
        if (patternNode.getPattern().isItObjectWhat()) {
            What objectWhat = (What) patternNode.getPattern().getObject();
            this.objectDependencyBoard.put(objectWhat.getName(), patternQuery.getTable());

        }
    }

    private void excecuteSubject(PatternNode patternNode, PatternQuery patternQuery) {
        if (patternNode.getPattern().isItSubjectWhat()) {
            What subjectWhat = (What) patternNode.getPattern().getSubject();
            this.subjectDependencyBoard.put(subjectWhat.getName(), patternQuery.getTable());
        }
    }

    public void sortNode(FunctionNode functionNode) {
        List<Node> sortedList = getSortedNodeList(functionNode);
        for (int i = 0; i < functionNode.getChildNumber(); i++) {
            functionNode.changeNode(i, sortedList.get(i));
        }
    }

    private List<Node> getSortedNodeList(FunctionNode functionNode) {
        List<Node> sortedList = new ArrayList();
        for (int i = 0; i <= 3; i++) {
            sortedList.addAll(getAllNodesOfOrder(i, functionNode));
        }
        sortedList.addAll(getAllOtherFunctionNodes(functionNode));
        return sortedList;
    }

    private List<Node> getAllNodesOfOrder(int order, FunctionNode functionNode) {
        List<Node> list = new ArrayList();
        for (int i = 0; i < functionNode.getChildNumber(); i++) {
            if (functionNode.getNode(i) instanceof PatternNode) {
                PatternNode patternNode = (PatternNode) functionNode.getNode(i);
                if (patternNode.getPattern().getOrder() == order) {
                    list.add(patternNode);
                }
            }
        }
        return list;
    }

    private List<Node> getAllOtherFunctionNodes(FunctionNode functionNode) {
        List<Node> list = new ArrayList();
        for (int i = 0; i < functionNode.getChildNumber(); i++) {
            if (functionNode.getNode(i) instanceof FunctionNode) {
                list.add(functionNode.getNode(i));
            }
        }
        return list;
    }
}
