package player.players;

import cubetriplestore.CubeTripleStoreQuery;
import managers.dependenciesManagers.NodeDependencyManager;
import managers.priorityManagers.NodePriorityManager;
import patternmatching.PatternQuery;
import player.Player;
import triskelTable.Table;
import triskeloperationtree.FunctionNode;
import triskeloperationtree.OperationTree;
import triskeloperationtree.PatternNode;
import triskeloperationtree.TableNode;

public class PatternQueryPlayer extends Player {

    private final CubeTripleStoreQuery cubeTripleStoreQuery;
    private final OperationTree operationTree;
    private final NodePriorityManager nodePriorityManager;
    private final NodeDependencyManager nodeDependencyManager;

    public PatternQueryPlayer(CubeTripleStoreQuery cubeTripleStoreQuery, OperationTree operationTree) {
        this.cubeTripleStoreQuery = cubeTripleStoreQuery;
        this.operationTree = operationTree;
        this.nodePriorityManager = new NodePriorityManager();
        this.nodeDependencyManager = new NodeDependencyManager(cubeTripleStoreQuery);

    }

    @Override
    public Table excecute() {
        if (operationTree.getRoot() instanceof FunctionNode) {
            FunctionNode functionNode = (FunctionNode) operationTree.getRoot();
            return executeTree(functionNode);
        }
        return null;
    }

    private Table executeTree(FunctionNode functionNode) {
        nodePriorityManager.excecute(functionNode);
        nodeDependencyManager.excecute(functionNode);
        executeNode(functionNode);
        return functionNode.execute();
    }

    private void executeNode(FunctionNode functionNode) {
        for (int i = 0; i < functionNode.getChildNumber(); i++) {
            executeNodeChild(functionNode, i);
        }
    }

    private void executeNodeChild(FunctionNode functionNode, int i) {
        if (functionNode.getNode(i) instanceof PatternNode) {
            executePattern(functionNode, i);
        } else if (functionNode.getNode(i) instanceof FunctionNode) {
            functionNode.changeNode(i, new TableNode(executeTree((FunctionNode) functionNode.getNode(i))));
        }
    }

    private void executePattern(FunctionNode functionNode, int i) {
        PatternNode patternNode = (PatternNode) functionNode.getNode(i);
        PatternQuery patternQuery = cubeTripleStoreQuery.createPatternQuery(patternNode.getPattern());
        patternQuery.execute();
        functionNode.changeNode(i, new TableNode(patternQuery.getTable()));
    }
}
