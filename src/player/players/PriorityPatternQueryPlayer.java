package player.players;

import player.Player;
import cubetriplestore.CubeTripleStoreQuery;
import java.util.ArrayList;
import java.util.List;
import patternmatching.PatternQuery;
import triskelTable.Table;
import triskeloperationtree.FunctionNode;
import triskeloperationtree.Node;
import triskeloperationtree.OperationTree;
import triskeloperationtree.PatternNode;
import triskeloperationtree.TableNode;

public class PriorityPatternQueryPlayer extends Player {

    private final CubeTripleStoreQuery cubeTripleStoreQuery;
    private final OperationTree operationTree;
    private final Table table;

    public PriorityPatternQueryPlayer(CubeTripleStoreQuery cubeTripleStoreQuery, OperationTree operationTree) {
        this.cubeTripleStoreQuery = cubeTripleStoreQuery;
        this.operationTree = operationTree;
        this.table = null;
    }

    @Override
    public Table excecute() {

        if (operationTree.getRoot() instanceof FunctionNode) {
            FunctionNode functionNode = (FunctionNode) operationTree.getRoot();
            return solvePattern(functionNode);
        }
        return this.table;
    }

    private Table solvePattern(FunctionNode functionNode) {
        sortNode(functionNode);
        for (int i = 0; i < functionNode.getChildNumber(); i++) {
            if (functionNode.getNode(i) instanceof PatternNode) {
                PatternNode patternNode = (PatternNode) functionNode.getNode(i);
                PatternQuery patternQuery = cubeTripleStoreQuery.createPatternQuery(patternNode.getPattern());
                patternQuery.execute();
                functionNode.changeNode(i, new TableNode(patternQuery.getTable()));
            } else {
                functionNode.changeNode(i, new TableNode(solvePattern((FunctionNode) functionNode.getNode(i))));
            }
        }
        return functionNode.execute();
    }

    public void sortNode(FunctionNode functionNode) {
        List<Node> sortedList = getSortedNodeList(functionNode);
        for (int i = 0; i < functionNode.getChildNumber(); i++) {
            functionNode.changeNode(i, sortedList.get(i));
        }
    }

    private List<Node> getSortedNodeList(FunctionNode functionNode) {
        List<Node> sortedList = new ArrayList();
        for (int i = 0; i <= 3; i++) {
            sortedList.addAll(getAllNodesOfOrder(i, functionNode));
        }
        sortedList.addAll(getAllOtherFunctionNodes(functionNode));
        return sortedList;
    }

    private List<Node> getAllNodesOfOrder(int order, FunctionNode functionNode) {
        List<Node> list = new ArrayList();
        for (int i = 0; i < functionNode.getChildNumber(); i++) {
            if (functionNode.getNode(i) instanceof PatternNode) {
                PatternNode patternNode = (PatternNode) functionNode.getNode(i);
                if (patternNode.getPattern().getOrder() == order) {
                    list.add(patternNode);
                }
            }
        }
        return list;
    }

    private List<Node> getAllOtherFunctionNodes(FunctionNode functionNode) {
        List<Node> list = new ArrayList();
        for (int i = 0; i < functionNode.getChildNumber(); i++) {
            if (functionNode.getNode(i) instanceof FunctionNode) {
                list.add(functionNode.getNode(i));
            }
        }
        return list;
    }
}
