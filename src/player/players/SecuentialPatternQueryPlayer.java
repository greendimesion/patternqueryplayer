package player.players;

import player.Player;
import cubetriplestore.CubeTripleStoreQuery;
import patternmatching.PatternQuery;
import triskelTable.Table;
import triskeloperationtree.FunctionNode;
import triskeloperationtree.OperationTree;
import triskeloperationtree.PatternNode;
import triskeloperationtree.TableNode;

public class SecuentialPatternQueryPlayer extends Player {

    private final CubeTripleStoreQuery cubeTripleStoreQuery;
    private final OperationTree operationTree;
    private final Table table;

    public SecuentialPatternQueryPlayer(CubeTripleStoreQuery cubeTripleStoreQuery, OperationTree operationTree) {
        this.cubeTripleStoreQuery = cubeTripleStoreQuery;
        this.operationTree = operationTree;
        this.table = null;
    }

    @Override
    public Table excecute() {

        if (operationTree.getRoot() instanceof FunctionNode) {
            FunctionNode functionNode = (FunctionNode) operationTree.getRoot();
            return solvePattern(functionNode);
        }
        return this.table;
    }

    private Table solvePattern(FunctionNode functionNode) {
        for (int i = 0; i < functionNode.getChildNumber(); i++) {
            if (functionNode.getNode(i) instanceof PatternNode) {
                PatternNode patternNode = (PatternNode) functionNode.getNode(i);
                PatternQuery patternQuery = cubeTripleStoreQuery.createPatternQuery(patternNode.getPattern());
                patternQuery.execute();
                functionNode.changeNode(i, new TableNode(patternQuery.getTable()));
            } else {
                functionNode.changeNode(i, new TableNode(solvePattern((FunctionNode) functionNode.getNode(i))));
            }
        }
        return functionNode.execute();
    }
}
