package players.withDependencies;

import cubetriplestore.CubeTripleStoreQuery;
import dataPoolForTest.ForestOfTreesWithDependencies;
import dataPoolForTest.StorageForTests;
import static org.junit.Assert.*;
import org.junit.Test;
import player.players.PriorityDependenciesPatternQueryPlayer;
import player.players.PriorityPatternQueryPlayer;
import triskelTable.Table;
import triskeloperationtree.OperationTree;

public class TreesWithDependenciesSubject extends StorageForTests {

    private final ForestOfTreesWithDependencies trees;

    public TreesWithDependenciesSubject() {
        this.trees = new ForestOfTreesWithDependencies();
    }

    @Test
    public void treeWithNoDependencies() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());
        OperationTree operationTree = trees.noDependeciesTree();
        PriorityDependenciesPatternQueryPlayer priorityDependenciesPatternQueryPlayer = new PriorityDependenciesPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
        Table table = priorityDependenciesPatternQueryPlayer.excecute();
        assertEquals(null, table);
    }

    @Test
    public void treeWithNoBidimensionalPattern() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());
        OperationTree operationTree = trees.noBidimensionalSearch();
        PriorityDependenciesPatternQueryPlayer priorityDependenciesPatternQueryPlayer = new PriorityDependenciesPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
        Table table = priorityDependenciesPatternQueryPlayer.excecute();
        assertEquals(null, table);
    }

    @Test
    public void treeWithSubjectDependency() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());
        OperationTree operationTree = trees.subjectDependencyTree();
        PriorityDependenciesPatternQueryPlayer priorityDependenciesPatternQueryPlayer = new PriorityDependenciesPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
        Table tableObtained = priorityDependenciesPatternQueryPlayer.excecute();
        operationTree = trees.subjectDependencyTree();
        PriorityPatternQueryPlayer priorityPatternQueryPlayer = new PriorityPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
        Table tableExpeted = priorityPatternQueryPlayer.excecute();
        assertEquals(tableObtained.size(), tableExpeted.size());
        assertEquals(tableObtained.getColumns()[0], tableExpeted.getColumns()[0]);
        assertEquals(tableObtained.getColumns()[1], tableExpeted.getColumns()[1]);
        assertEquals(tableObtained.getRow(0).length, tableExpeted.getRow(0).length);
    }

    @Test
    public void treeWithSubjectDependencyTwoMethodResult() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());

        OperationTree operationTree = trees.subjectObjectDependencyTree();
        PriorityDependenciesPatternQueryPlayer priorityDependenciesPatternQueryPlayer = new PriorityDependenciesPatternQueryPlayer(cubeTripleStoreQuery, operationTree);

        Table tableObtained = priorityDependenciesPatternQueryPlayer.excecute();

        operationTree = trees.subjectObjectDependencyTree();
        PriorityPatternQueryPlayer priorityPatternQueryPlayer = new PriorityPatternQueryPlayer(cubeTripleStoreQuery, operationTree);

        Table tableExpeted = priorityPatternQueryPlayer.excecute();

        assertEquals(tableObtained.size(), tableExpeted.size());
        assertEquals(tableObtained.getColumns()[0], tableExpeted.getColumns()[0]);
        assertEquals(tableObtained.getColumns()[1], tableExpeted.getColumns()[1]);
        assertEquals(tableObtained.getRow(0).length, tableExpeted.getRow(0).length);
    }

    @Test
    public void treeWithSubjectDependencyMoreNodes() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());
        OperationTree operationTree = trees.subjectDependencyTreeMoreNodes();
        PriorityDependenciesPatternQueryPlayer priorityDependenciesPatternQueryPlayer = new PriorityDependenciesPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
        Table tableObtained = priorityDependenciesPatternQueryPlayer.excecute();
        assertEquals(tableObtained.getColumns()[0], "Name");
        assertEquals(tableObtained.getColumns()[1], "Have");
        assertEquals(tableObtained.getRow(0).length, 2);
    }
}
