package players.withDependencies;

import cubetriplestore.CubeTripleStoreQuery;
import dataPoolForTest.ForestOfTreesWithDependencies;
import dataPoolForTest.StorageForTests;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import player.players.PriorityDependenciesPatternQueryPlayer;
import player.players.PriorityPatternQueryPlayer;
import triskelTable.Table;
import triskeloperationtree.OperationTree;

public class TreesWithDependenciesObject extends StorageForTests {

    private final ForestOfTreesWithDependencies trees;

    public TreesWithDependenciesObject() {
        this.trees = new ForestOfTreesWithDependencies();
    }

    @Test
    public void treeWithObjectObjectDependency() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());
        OperationTree operationTree = trees.objectObjectDependencyTree();
        PriorityDependenciesPatternQueryPlayer priorityDependenciesPatternQueryPlayer = new PriorityDependenciesPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
        Table tableObtained = priorityDependenciesPatternQueryPlayer.excecute();
        assertEquals(tableObtained.size(), 3);
        assertEquals(tableObtained.getColumns().length, 2);
    }

    @Test
    public void treeWithObjectDependencyTwoMethodResult() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());

        OperationTree operationTree = trees.objectObjectDependencyTree();
        PriorityDependenciesPatternQueryPlayer priorityDependenciesPatternQueryPlayer = new PriorityDependenciesPatternQueryPlayer(cubeTripleStoreQuery, operationTree);

        Table tableObtained = priorityDependenciesPatternQueryPlayer.excecute();

        operationTree = trees.objectObjectDependencyTree();
        PriorityPatternQueryPlayer priorityPatternQueryPlayer = new PriorityPatternQueryPlayer(cubeTripleStoreQuery, operationTree);

        Table tableExpeted = priorityPatternQueryPlayer.excecute();

        assertEquals(tableObtained.size(), tableExpeted.size());
        assertEquals(tableObtained.getColumns()[0], tableExpeted.getColumns()[1]);
        assertEquals(tableObtained.getColumns()[1], tableExpeted.getColumns()[0]);
        assertEquals(tableObtained.getRow(0).length, tableExpeted.getRow(0).length);
    }

}
