package players.sequential;

import cubetriplestore.CubeTripleStoreQuery;
import dataPoolForTest.ForestOfComplexTrees;
import dataPoolForTest.StorageForTests;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import player.players.SecuentialPatternQueryPlayer;
import triskelTable.Table;
import triskeloperationtree.OperationTree;

public class UnidimensionalPatterns extends StorageForTests {

    private final ForestOfComplexTrees trees;

    public UnidimensionalPatterns() {
        this.trees = new ForestOfComplexTrees();
    }

    @Test
    public void aWhatInSubjectPlace() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());
        OperationTree operationTree = trees.treeWithSubjectWhat();
        SecuentialPatternQueryPlayer patternQueryPlayer = new SecuentialPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
        Table table = patternQueryPlayer.excecute();
        assertEquals(1, table.getColumns().length);
        assertEquals(3, table.size());
    }

    @Test
    public void aWhatInPredicatePlace() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());
        OperationTree operationTree = trees.treeWithPredicateWhat();
        SecuentialPatternQueryPlayer patternQueryPlayer = new SecuentialPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
        Table table = patternQueryPlayer.excecute();
        assertEquals(1, table.getColumns().length);
        assertEquals(1, table.size());
    }

    @Test
    public void aWhatInObjectPlace() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());
        OperationTree operationTree = trees.treeWithObjectWhat();
        SecuentialPatternQueryPlayer patternQueryPlayer = new SecuentialPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
        Table table = patternQueryPlayer.excecute();
        assertEquals(1, table.getColumns().length);
        assertEquals(1, table.size());
    }
}
