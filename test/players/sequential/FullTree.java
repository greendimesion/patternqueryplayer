package players.sequential;

import cubetriplestore.CubeTripleStoreQuery;
import dataPoolForTest.ForestOfTrueTrees;
import dataPoolForTest.StorageForTests;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import player.players.SecuentialPatternQueryPlayer;
import triskelTable.Table;
import triskeloperationtree.OperationTree;

public class FullTree extends StorageForTests {

    private final ForestOfTrueTrees trees;

    public FullTree() {
        this.trees = new ForestOfTrueTrees();
    }

    @Test
    public void twoJoinsTree() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());
        OperationTree operationTree = trees.twoJoins();
        SecuentialPatternQueryPlayer patternQueryPlayer = new SecuentialPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
        Table table = patternQueryPlayer.excecute();
        assertEquals(1, table.getColumns().length);
        assertEquals(3, table.size());
    }

    @Test
    public void twoJoinsTreeBidimensional() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());
        OperationTree operationTree = trees.twoJoinsBidimensional();
        SecuentialPatternQueryPlayer patternQueryPlayer = new SecuentialPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
        Table table = patternQueryPlayer.excecute();
        assertEquals(3, table.getColumns().length);
        assertEquals(5, table.size());
    }

    @Test
    public void authorQuery() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());
        OperationTree operationTree = trees.authorTreeQuery();
        SecuentialPatternQueryPlayer patternQueryPlayer = new SecuentialPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
        Table table = patternQueryPlayer.excecute();
        assertEquals(3, table.getColumns().length);
        assertEquals(4, table.size());
        assertEquals("a", table.getColumns()[0]);
        assertEquals("title", table.getColumns()[1]);
        assertEquals("author", table.getColumns()[2]);
    }
}
