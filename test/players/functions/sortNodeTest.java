package players.functions;

import dataPoolForTest.PoolOfNodes;
import org.junit.Assert;
import org.junit.Test;
import player.players.PriorityPatternQueryPlayer;
import triskeloperationtree.FunctionNode;
import triskeloperationtree.JoinNode;
import triskeloperationtree.PatternNode;

public class sortNodeTest {

    private final PoolOfNodes poolOfNodes;

    public sortNodeTest() {
        this.poolOfNodes = new PoolOfNodes();
    }

    @Test
    public void sortOnlyPatternsNodeOrderZero() {
        JoinNode joinNode = (JoinNode) poolOfNodes.initTestNodeOrderZero();
        PriorityPatternQueryPlayer patternQueryPlayer = new PriorityPatternQueryPlayer(null, null);
        patternQueryPlayer.sortNode(joinNode);
        Assert.assertEquals(3, joinNode.getChildNumber());
    }

    @Test
    public void sortOnlyPatternsNodeOrderZeroAndOne() {
        JoinNode joinNode = (JoinNode) poolOfNodes.initTestNodeOrderOne();
        PriorityPatternQueryPlayer patternQueryPlayer = new PriorityPatternQueryPlayer(null, null);
        patternQueryPlayer.sortNode(joinNode);
        Assert.assertEquals(3, joinNode.getChildNumber());
        if (joinNode.getNode(2) instanceof PatternNode) {
            PatternNode patternNode = (PatternNode) joinNode.getNode(2);
            Assert.assertEquals(1, patternNode.getPattern().getOrder());
        }
    }

    @Test
    public void sortOnlyPatternsNodeWithUpToOrderTwo() {
        JoinNode joinNode = (JoinNode) poolOfNodes.initTestNodeOrderTwo();
        PriorityPatternQueryPlayer patternQueryPlayer = new PriorityPatternQueryPlayer(null, null);
        patternQueryPlayer.sortNode(joinNode);
        Assert.assertEquals(4, joinNode.getChildNumber());
        if (joinNode.getNode(3) instanceof PatternNode) {
            PatternNode patternNode = (PatternNode) joinNode.getNode(3);
            Assert.assertEquals(2, patternNode.getPattern().getOrder());
        }
    }

    @Test
    public void sortOnlyPatternsNodeWithUpToOrderThree() {
        JoinNode joinNode = (JoinNode) poolOfNodes.initTestNodeOrderThree();
        PriorityPatternQueryPlayer patternQueryPlayer = new PriorityPatternQueryPlayer(null, null);
        patternQueryPlayer.sortNode(joinNode);
        Assert.assertEquals(5, joinNode.getChildNumber());
        if (joinNode.getNode(4) instanceof PatternNode) {
            PatternNode patternNode = (PatternNode) joinNode.getNode(4);
            Assert.assertEquals(3, patternNode.getPattern().getOrder());
        }
    }

    @Test
    public void sortOnlyPatternsNodeWithOtherFuctionNodes() {
        JoinNode joinNode = (JoinNode) poolOfNodes.initTestNodeOrderThree();
        PriorityPatternQueryPlayer patternQueryPlayer = new PriorityPatternQueryPlayer(null, null);
        patternQueryPlayer.sortNode(joinNode);
        Assert.assertEquals(5, joinNode.getChildNumber());
        if (joinNode.getNode(4) instanceof FunctionNode) {
            Assert.assertEquals("JoinNode", joinNode.getNode(4).getClass());
        }
    }
}
