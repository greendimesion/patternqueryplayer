package players.MultiLevel;

import cubetriplestore.CubeTripleStoreQuery;
import dataPoolForTest.ForestOfTreesWithDependencies;
import dataPoolForTest.StorageForTests;
import static org.junit.Assert.*;
import org.junit.Test;
import player.players.MultiLevelDependencyPlayer;
import player.players.PriorityPatternQueryPlayer;
import triskelTable.Table;
import triskeloperationtree.OperationTree;

public class TreesWithDependenciesSubject extends StorageForTests {

    private final ForestOfTreesWithDependencies trees;

    public TreesWithDependenciesSubject() {
        this.trees = new ForestOfTreesWithDependencies();
    }

    @Test
    public void treeWithNoDependencies() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());
        OperationTree operationTree = trees.noDependeciesTree();
        MultiLevelDependencyPlayer multiLevelDependencyPlayer = new MultiLevelDependencyPlayer(cubeTripleStoreQuery, operationTree);
        Table table = multiLevelDependencyPlayer.excecute();
        assertEquals(null, table);
    }

    @Test
    public void treeWithNoBidimensionalPattern() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());
        OperationTree operationTree = trees.noBidimensionalSearch();
        MultiLevelDependencyPlayer multiLevelDependencyPlayer = new MultiLevelDependencyPlayer(cubeTripleStoreQuery, operationTree);
        Table table = multiLevelDependencyPlayer.excecute();
        assertEquals(null, table);
    }

    @Test
    public void treeWithSubjectDependency() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());
        OperationTree operationTree = trees.subjectDependencyTree();
        MultiLevelDependencyPlayer multiLevelDependencyPlayer = new MultiLevelDependencyPlayer(cubeTripleStoreQuery, operationTree);
        Table tableObtained = multiLevelDependencyPlayer.excecute();
        operationTree = trees.subjectDependencyTree();
        PriorityPatternQueryPlayer priorityPatternQueryPlayer = new PriorityPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
        Table tableExpeted = priorityPatternQueryPlayer.excecute();
        assertEquals(tableObtained.size(), tableExpeted.size());
        assertEquals(tableObtained.getColumns()[0], tableExpeted.getColumns()[0]);
        assertEquals(tableObtained.getColumns()[1], tableExpeted.getColumns()[1]);
        assertEquals(tableObtained.getRow(0).length, tableExpeted.getRow(0).length);
    }

    @Test
    public void treeWithSubjectDependencyTwoMethodResult() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());

        OperationTree operationTree = trees.subjectObjectDependencyTree();
        MultiLevelDependencyPlayer multiLevelDependencyPlayer = new MultiLevelDependencyPlayer(cubeTripleStoreQuery, operationTree);

        Table tableObtained = multiLevelDependencyPlayer.excecute();

        operationTree = trees.subjectObjectDependencyTree();
        PriorityPatternQueryPlayer priorityPatternQueryPlayer = new PriorityPatternQueryPlayer(cubeTripleStoreQuery, operationTree);

        Table tableExpeted = priorityPatternQueryPlayer.excecute();

        assertEquals(tableObtained.size(), tableExpeted.size());
        assertEquals(tableObtained.getColumns()[0], tableExpeted.getColumns()[0]);
        assertEquals(tableObtained.getColumns()[1], tableExpeted.getColumns()[1]);
        assertEquals(tableObtained.getRow(0).length, tableExpeted.getRow(0).length);
    }

    @Test
    public void treeWithSubjectDependencyMoreNodes() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());
        OperationTree operationTree = trees.subjectDependencyTreeMoreNodes();
        MultiLevelDependencyPlayer multiLevelDependencyPlayer = new MultiLevelDependencyPlayer(cubeTripleStoreQuery, operationTree);
        Table tableObtained = multiLevelDependencyPlayer.excecute();
        assertEquals(tableObtained.getColumns()[0], "Name");
        assertEquals(tableObtained.getColumns()[1], "Have");
        assertEquals(tableObtained.getRow(0).length, 2);
    }
}
