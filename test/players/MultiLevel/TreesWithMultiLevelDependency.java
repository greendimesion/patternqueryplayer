package players.MultiLevel;

import cubetriplestore.CubeTripleStoreQuery;
import dataPoolForTest.ForestOfTreesWithDependencies;
import dataPoolForTest.ForestOfTreesWithMultiLevelDependencies;
import dataPoolForTest.StorageForTests;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import player.players.MultiLevelDependencyPlayer;
import player.players.PriorityPatternQueryPlayer;
import triskelTable.Table;
import triskeloperationtree.OperationTree;

public class TreesWithMultiLevelDependency extends StorageForTests {

    private final ForestOfTreesWithMultiLevelDependencies trees;

    public TreesWithMultiLevelDependency() {
        this.trees = new ForestOfTreesWithMultiLevelDependencies();
    }

    @Test
    public void SubjectDependencyInFirstLevel() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());
        OperationTree operationTree = trees.FirstLevelDependencyTree();

        MultiLevelDependencyPlayer multiLevelDependencyPlayer = new MultiLevelDependencyPlayer(cubeTripleStoreQuery, operationTree);
        Table tableObtained = multiLevelDependencyPlayer.excecute();

        operationTree = trees.FirstLevelDependencyTree();
        PriorityPatternQueryPlayer priorityPatternQueryPlayer = new PriorityPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
        Table tableExpeted = priorityPatternQueryPlayer.excecute();
        assertEquals(tableExpeted.size(), tableObtained.size());
        assertEquals(tableObtained.getColumns()[0],tableExpeted.getColumns()[0]);
        assertEquals(tableObtained.getColumns()[1],tableExpeted.getColumns()[1]);
    }
}
