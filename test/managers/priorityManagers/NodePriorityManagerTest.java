package managers.priorityManagers;

import dataPoolForTest.PoolOfNodes;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import triskeloperationtree.FunctionNode;
import triskeloperationtree.JoinNode;
import triskeloperationtree.PatternNode;

public class NodePriorityManagerTest {

    private final PoolOfNodes poolOfNodes;

    public NodePriorityManagerTest() {
        this.poolOfNodes = new PoolOfNodes();
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void sortOnlyPatternsNodeOrderZero() {
        JoinNode joinNode = (JoinNode) poolOfNodes.initTestNodeOrderZero();
        NodePriorityManager nodePriorityManager = new NodePriorityManager();
        nodePriorityManager.excecute(joinNode);
        Assert.assertEquals(3, joinNode.getChildNumber());
    }

    @Test
    public void sortOnlyPatternsNodeOrderZeroAndOne() {
        JoinNode joinNode = (JoinNode) poolOfNodes.initTestNodeOrderOne();
        NodePriorityManager nodePriorityManager = new NodePriorityManager();
        nodePriorityManager.excecute(joinNode);
        Assert.assertEquals(3, joinNode.getChildNumber());
        if (joinNode.getNode(2) instanceof PatternNode) {
            PatternNode patternNode = (PatternNode) joinNode.getNode(2);
            Assert.assertEquals(1, patternNode.getPattern().getOrder());
        }
    }

    @Test
    public void sortOnlyPatternsNodeWithUpToOrderTwo() {
        JoinNode joinNode = (JoinNode) poolOfNodes.initTestNodeOrderTwo();
        NodePriorityManager nodePriorityManager = new NodePriorityManager();
        nodePriorityManager.excecute(joinNode);
        Assert.assertEquals(4, joinNode.getChildNumber());
        if (joinNode.getNode(3) instanceof PatternNode) {
            PatternNode patternNode = (PatternNode) joinNode.getNode(3);
            Assert.assertEquals(2, patternNode.getPattern().getOrder());
        }
    }

    @Test
    public void sortOnlyPatternsNodeWithUpToOrderThree() {
        JoinNode joinNode = (JoinNode) poolOfNodes.initTestNodeOrderThree();
        NodePriorityManager nodePriorityManager = new NodePriorityManager();
        nodePriorityManager.excecute(joinNode);
        Assert.assertEquals(5, joinNode.getChildNumber());
        if (joinNode.getNode(4) instanceof PatternNode) {
            PatternNode patternNode = (PatternNode) joinNode.getNode(4);
            Assert.assertEquals(3, patternNode.getPattern().getOrder());
        }
    }

    @Test
    public void sortOnlyPatternsNodeWithOtherFuctionNodes() {
        JoinNode joinNode = (JoinNode) poolOfNodes.initTestNodeOrderThree();
        NodePriorityManager nodePriorityManager = new NodePriorityManager();
        nodePriorityManager.excecute(joinNode);
        Assert.assertEquals(5, joinNode.getChildNumber());
        if (joinNode.getNode(4) instanceof FunctionNode) {
            Assert.assertEquals("JoinNode", joinNode.getNode(4).getClass());
        }
    }
}
