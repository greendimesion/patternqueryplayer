package dataPoolForTest;

import patterns.Constant;
import patterns.Pattern;
import patterns.What;
import triskeloperationtree.JoinNode;
import triskeloperationtree.OperationTree;
import triskeloperationtree.PatternNode;

public class ForestOfTreesWithDependencies {

    public OperationTree noDependeciesTree() {
        OperationTree operationTree = new OperationTree();
        JoinNode joinNode = new JoinNode();
        Pattern pattern = new Pattern(new What("Name"), new Constant(0), new Constant(8));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new What("Code"), new Constant(0), new Constant(8));
        joinNode.addNode(new PatternNode(pattern));
        operationTree.setRoot(joinNode);
        return operationTree;
    }

    public OperationTree noBidimensionalSearch() {
        OperationTree operationTree = new OperationTree();
        JoinNode joinNode = new JoinNode();
        Pattern pattern = new Pattern(new What("Name"), new Constant(0), new Constant(8));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new What("Code"), new Constant(0), new Constant(8));
        joinNode.addNode(new PatternNode(pattern));
        operationTree.setRoot(joinNode);
        return operationTree;
    }

    public OperationTree subjectDependencyTree() {
        OperationTree operationTree = new OperationTree();
        JoinNode joinNode = new JoinNode();
        Pattern pattern = new Pattern(new What("Name"), new Constant(0), new Constant(8));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new What("Name"), new Constant(0), new What("Foreing"));
        joinNode.addNode(new PatternNode(pattern));
        operationTree.setRoot(joinNode);
        return operationTree;
    }

    public OperationTree subjectDependencyTreeMoreNodes() {
        OperationTree operationTree = new OperationTree();
        JoinNode joinNode = new JoinNode();
        Pattern pattern = new Pattern(new What("Name"), new Constant(0), new Constant(8));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new What("Name"), new Constant(0), new Constant(8));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new What("Name"), new What("Have"), new Constant(8));
        joinNode.addNode(new PatternNode(pattern));
        operationTree.setRoot(joinNode);
        return operationTree;
    }

    public OperationTree subjectObjectDependencyTree() {
        OperationTree operationTree = new OperationTree();
        JoinNode joinNode = new JoinNode();
        Pattern pattern = new Pattern(new What("Name"), new Constant(0), new Constant(8));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new What("Code"), new Constant(0), new What("Name"));
        joinNode.addNode(new PatternNode(pattern));
        operationTree.setRoot(joinNode);
        return operationTree;
    }

    public OperationTree objectObjectDependencyTree() {
        OperationTree operationTree = new OperationTree();
        JoinNode joinNode = new JoinNode();
        Pattern pattern = new Pattern(new Constant(1), new Constant(0), new What("Foreing"));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new What("Name"), new Constant(0), new What("Foreing"));
        joinNode.addNode(new PatternNode(pattern));
        operationTree.setRoot(joinNode);
        return operationTree;
    }
}
