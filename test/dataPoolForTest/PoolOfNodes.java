package dataPoolForTest;

import patterns.Constant;
import patterns.Pattern;
import patterns.What;
import triskeloperationtree.FunctionNode;
import triskeloperationtree.JoinNode;
import triskeloperationtree.PatternNode;

public class PoolOfNodes {

    public FunctionNode initTestNodeOrderZero() {
        JoinNode joinNode = new JoinNode();
        Pattern pattern = new Pattern(new Constant(1), new Constant(1), new Constant(1));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new Constant(1), new Constant(2), new Constant(1));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new Constant(1), new Constant(2), new Constant(3));
        joinNode.addNode(new PatternNode(pattern));
        return joinNode;
    }

    public FunctionNode initTestNodeOrderOne() {
        JoinNode joinNode = new JoinNode();
        Pattern pattern = new Pattern(new Constant(1), new Constant(1), new Constant(1));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new Constant(1), new Constant(2), new What("Object"));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new Constant(1), new Constant(2), new Constant(3));
        joinNode.addNode(new PatternNode(pattern));
        return joinNode;
    }

    public FunctionNode initTestNodeOrderTwo() {
        JoinNode joinNode = new JoinNode();
        Pattern pattern = new Pattern(new Constant(1), new Constant(1), new Constant(1));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new Constant(1), new Constant(2), new What("Object"));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new What("suject"), new Constant(2), new What("Object"));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new Constant(2), new Constant(2), new What("Object"));
        joinNode.addNode(new PatternNode(pattern));
        return joinNode;
    }

    public FunctionNode initTestNodeOrderThree() {
        JoinNode joinNode = new JoinNode();
        Pattern pattern = new Pattern(new Constant(1), new Constant(1), new Constant(1));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new Constant(1), new Constant(2), new What("Object"));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new What("suject"), new Constant(2), new What("Object"));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new Constant(2), new Constant(2), new What("Object"));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new What("Subject"), new What("Predicate"), new What("Object"));
        joinNode.addNode(new PatternNode(pattern));
        return joinNode;
    }

    public FunctionNode initTestNodeWithFuctionNodes() {
        JoinNode joinNode = new JoinNode();
        Pattern pattern = new Pattern(new Constant(1), new Constant(1), new Constant(1));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new Constant(1), new Constant(2), new What("Object"));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new What("suject"), new Constant(2), new What("Object"));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new Constant(2), new Constant(2), new What("Object"));
        joinNode.addNode(new PatternNode(pattern));
        pattern = new Pattern(new What("Subject"), new What("Predicate"), new What("Object"));
        joinNode.addNode(new PatternNode(pattern));
        JoinNode joinNodeSon = new JoinNode();
        pattern = new Pattern(new What("Subject"), new What("Predicate"), new What("Object"));
        joinNodeSon.addNode(new PatternNode(pattern));
        joinNode.addNode(joinNodeSon);
        return joinNode;
    }
}
