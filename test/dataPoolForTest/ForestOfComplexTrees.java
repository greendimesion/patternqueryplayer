package dataPoolForTest;

import patterns.Constant;
import patterns.Pattern;
import patterns.What;
import triskeloperationtree.JoinNode;
import triskeloperationtree.OperationTree;
import triskeloperationtree.PatternNode;

public class ForestOfComplexTrees {

    public OperationTree treeWithSubjectWhat() {
        OperationTree operationTree = new OperationTree();
        JoinNode joinNode = new JoinNode();
        Pattern pattern = new Pattern(new What("Subject"), new Constant(0), new Constant(8));
        joinNode.addNode(new PatternNode(pattern));
        operationTree.setRoot(joinNode);
        return operationTree;
    }

    public OperationTree treeWithPredicateWhat() {
        OperationTree operationTree = new OperationTree();
        JoinNode joinNode = new JoinNode();
        Pattern pattern = new Pattern(new Constant(1), new What("Predicate"), new Constant(8));
        joinNode.addNode(new PatternNode(pattern));
        operationTree.setRoot(joinNode);
        return operationTree;
    }

    public OperationTree treeWithObjectWhat() {
        OperationTree operationTree = new OperationTree();
        JoinNode joinNode = new JoinNode();
        Pattern pattern = new Pattern(new Constant(1), new Constant(0), new What("Object"));
        joinNode.addNode(new PatternNode(pattern));
        operationTree.setRoot(joinNode);
        return operationTree;
    }

    public OperationTree treeWithSubjectyObjectWhat() {
        OperationTree operationTree = new OperationTree();
        JoinNode joinNode = new JoinNode();
        Pattern pattern = new Pattern(new What("Subject"), new Constant(0), new What("Object"));
        joinNode.addNode(new PatternNode(pattern));
        operationTree.setRoot(joinNode);
        return operationTree;
    }
}
