package dataPoolForTest;

import patterns.Constant;
import patterns.Pattern;
import patterns.What;
import triskeloperationtree.JoinNode;
import triskeloperationtree.OperationTree;
import triskeloperationtree.PatternNode;

public class ForestOfTreesWithMultiLevelDependencies {

    //     (joinN)[r]
    //         |
    //     _____________
    //     |           |          
    // (patternN)  (JoinN)
    //                 |
    //           _____________
    //           |           |
    //       (patternN)  (patternN)
    public OperationTree FirstLevelDependencyTree() {
        OperationTree operationTree = new OperationTree();
        JoinNode joinNodeFather = new JoinNode();
        JoinNode joinNodeSon = new JoinNode();
        Pattern pattern = new Pattern(new What("Name"), new Constant(0), new Constant(8));
        joinNodeFather.addNode(new PatternNode(pattern));
        pattern = new Pattern(new What("Name"), new Constant(0), new What("Foreing"));
        joinNodeSon.addNode(new PatternNode(pattern));
        pattern = new Pattern(new Constant(1), new Constant(0), new What("Foreing"));
        joinNodeSon.addNode(new PatternNode(pattern));
        joinNodeFather.addNode(joinNodeSon);
        operationTree.setRoot(joinNodeFather);
        return operationTree;
    }

    public OperationTree SecondLevelDependencyTree() {
        return null;
    }

}
