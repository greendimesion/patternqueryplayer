package dataPoolForTest;

import patterns.Constant;
import patterns.Pattern;
import triskeloperationtree.JoinNode;
import triskeloperationtree.OperationTree;
import triskeloperationtree.PatternNode;

public class ForestOfSimpleTrees {

    //        (joinN)[r]
    //            |
    //        (patternN)
    public OperationTree createOneChildTree() {
        OperationTree operationTree = new OperationTree();
        JoinNode joinNode = new JoinNode();
        joinNode.addNode(new PatternNode(initPattern(1, 0, 8)));
        operationTree.setRoot(joinNode);
        return operationTree;
    }

    //        (joinN)[r]
    //           |
    //     _____________
    //     |           |     
    // (patternN)  (patternN)
    public OperationTree createTwoChildTree() {
        OperationTree operationTree = new OperationTree();
        JoinNode joinNode = new JoinNode();
        joinNode.addNode(new PatternNode(initPattern(3, 0, 6)));
        joinNode.addNode(new PatternNode(initPattern(4, 0, 5)));
        operationTree.setRoot(joinNode);
        return operationTree;
    }

    //             (joinN)[r]
    //                 |
    //     ________________________
    //     |           |          |
    // (patternN)  (patternN) (patternN)
    public OperationTree createThreeChildTree() {
        OperationTree operationTree = new OperationTree();
        JoinNode joinNode = new JoinNode();
        joinNode.addNode(new PatternNode(initPattern(1, 2, 3)));
        joinNode.addNode(new PatternNode(initPattern(4, 5, 6)));
        joinNode.addNode(new PatternNode(initPattern(7, 8, 9)));
        operationTree.setRoot(joinNode);
        return operationTree;
    }

    private Pattern initPattern(int subject, int predicate, int Object) {
        return new Pattern(new Constant(subject), new Constant(predicate), new Constant(Object));
    }
}
