package dataPoolForTest;

import org.junit.Test;
import triple.Triple;
import cubetriplestore.CubeTripleStore;

public class StorageForTests {

    private CubeTripleStore store;

    public CubeTripleStore initStore() {
        store = new CubeTripleStore();
        store.assertTriple(new Triple(1, 0, 8));
        store.assertTriple(new Triple(2, 0, 7));
        store.assertTriple(new Triple(2, 0, 8));
        store.assertTriple(new Triple(3, 0, 6));
        store.assertTriple(new Triple(4, 0, 5));
        store.assertTriple(new Triple(5, 0, 4));
        store.assertTriple(new Triple(6, 0, 3));
        store.assertTriple(new Triple(6, 0, 8));
        store.assertTriple(new Triple(7, 0, 2));
        store.assertTriple(new Triple(8, 0, 1));
        initAuthorTriplets();
        initTitleTriplets();
        return store;
    }

    @Test
    public void patternTest() {
        return;
    }

    private void initAuthorTriplets() {
        store.assertTriple(new Triple(1, 1, 20));
        store.assertTriple(new Triple(2, 1, 30));
        store.assertTriple(new Triple(5, 1, 80));
        store.assertTriple(new Triple(8, 1, 19));
        store.assertTriple(new Triple(9, 1, 39));
        store.assertTriple(new Triple(4, 1, 91));
    }

    private void initTitleTriplets() {
        store.assertTriple(new Triple(1, 2, 120));
        store.assertTriple(new Triple(2, 2, 130));
        store.assertTriple(new Triple(9, 2, 139));
        store.assertTriple(new Triple(7, 2, 291));
        store.assertTriple(new Triple(4, 2, 191));
    }

}
