package dataPoolForTest;

import patterns.Constant;
import patterns.Pattern;
import patterns.What;
import triskeloperationtree.JoinNode;
import triskeloperationtree.OperationTree;
import triskeloperationtree.PatternNode;

public class ForestOfTrueTrees {

    public OperationTree twoJoins() {
        OperationTree operationTree = new OperationTree();
        JoinNode joinNodeFather = new JoinNode();
        JoinNode joinNodeSon = new JoinNode();

        Pattern pattern = new Pattern(new What("Subject"), new Constant(0), new Constant(8));
        joinNodeFather.addNode(new PatternNode(pattern));

        pattern = new Pattern(new What("Subject"), new Constant(0), new Constant(8));
        joinNodeSon.addNode(new PatternNode(pattern));
        pattern = new Pattern(new What("Subject"), new Constant(0), new Constant(8));
        joinNodeSon.addNode(new PatternNode(pattern));

        joinNodeFather.addNode(joinNodeSon);
        operationTree.setRoot(joinNodeFather);
        return operationTree;
    }

    public OperationTree twoJoinsBidimensional() {
        OperationTree operationTree = new OperationTree();
        JoinNode joinNodeFather = new JoinNode();
        JoinNode joinNodeSon = new JoinNode();

        Pattern pattern = new Pattern(new What("Subject"), new Constant(0), new Constant(8));
        joinNodeFather.addNode(new PatternNode(pattern));

        pattern = new Pattern(new What("Subject"), new Constant(0), new What("Object"));
        joinNodeSon.addNode(new PatternNode(pattern));
        pattern = new Pattern(new What("Subject"), new Constant(0), new What("Object"));
        joinNodeSon.addNode(new PatternNode(pattern));

        joinNodeFather.addNode(joinNodeSon);
        operationTree.setRoot(joinNodeFather);
        return operationTree;
    }

    public OperationTree authorTreeQuery() {
        OperationTree operationTree = new OperationTree();
        JoinNode joinNodeFather = new JoinNode();

        Pattern pattern = new Pattern(new What("a"), new Constant(1), new What("author"));
        joinNodeFather.addNode(new PatternNode(pattern));

        pattern = new Pattern(new What("a"), new Constant(2), new What("title"));
        joinNodeFather.addNode(new PatternNode(pattern));

        operationTree.setRoot(joinNodeFather);
        return operationTree;
    }
}
