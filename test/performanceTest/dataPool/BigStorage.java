package performanceTest.dataPool;

import cubetriplestore.CubeTripleStore;
import triple.Triple;

public class BigStorage {

    private CubeTripleStore store;

    public BigStorage() {
        this.store = new CubeTripleStore();
        load1kTriplets();
        load10kTriplets();
        load100kTriplets();
        load1kkTriplets();
    }

    private void load1kTriplets() {
        for (int i = 0; i < 1000; i++) {
            store.assertTriple(new Triple(i, 1, 8));
            store.assertTriple(new Triple(i, 2, 8));
            store.assertTriple(new Triple(1, 3, i));
        }
    }

    private void load10kTriplets() {
        for (int i = 0; i < 10000; i++) {
            store.assertTriple(new Triple(i, 4, 8));
            store.assertTriple(new Triple(i, 5, 8));
            store.assertTriple(new Triple(1, 6, i));
        }
    }

    private void load100kTriplets() {
        for (int i = 0; i < 100000; i++) {
            store.assertTriple(new Triple(i, 7, 8));
            store.assertTriple(new Triple(i, 8, 8));
            store.assertTriple(new Triple(1, 9, i));
        }
    }

    private void load1kkTriplets() {
        for (int i = 0; i < 1000000; i++) {
            store.assertTriple(new Triple(i, 10, 8));
            store.assertTriple(new Triple(i, 11, 8));
            store.assertTriple(new Triple(1, 12, i));
        }
    }

    public CubeTripleStore getStore() {
        return store;
    }
}
