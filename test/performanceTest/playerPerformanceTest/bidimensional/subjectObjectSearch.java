package performanceTest.playerPerformanceTest.bidimensional;

import cubetriplestore.CubeTripleStore;
import cubetriplestore.CubeTripleStoreQuery;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import org.junit.Before;
import org.junit.Test;
import patterns.Constant;
import patterns.Pattern;
import patterns.What;
import performanceTest.dataPool.BigStorage;
import player.players.MultiLevelDependencyPlayer;
import player.players.PriorityDependenciesPatternQueryPlayer;
import player.players.PriorityPatternQueryPlayer;
import player.players.SecuentialPatternQueryPlayer;
import triskelTable.Table;
import triskeloperationtree.JoinNode;
import triskeloperationtree.OperationTree;
import triskeloperationtree.PatternNode;

public class subjectObjectSearch {

    private BigStorage storage;

    @Before
    public void initCube() {
        storage = new BigStorage();
    }

    @Test
    public void SubjectSearch1k() throws FileNotFoundException, UnsupportedEncodingException, IOException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("1kLog.txt"), "utf-8"));
        writer.write("Secuent   Priority  Dependen  Multi");
        writer.newLine();
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(storage.getStore());
        CubeTripleStore cube = storage.getStore();
        long time_start1k, time_end1k;
        long time_start10k, time_end10k;
        long time_start100k, time_end100k;
        long time_start1kk, time_end1kk;
        OperationTree operationTree;

        for (int i = 0; i < 10000; i++) {
            operationTree = init1kTree();
            SecuentialPatternQueryPlayer sequientialPatternQueryPlayer = new SecuentialPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
            time_start1k = System.currentTimeMillis();
            Table sequientialTable = sequientialPatternQueryPlayer.excecute();
            time_end1k = System.currentTimeMillis();

            operationTree = init1kTree();
            PriorityPatternQueryPlayer priorityPatternQueryPlayer = new PriorityPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
            time_start10k = System.currentTimeMillis();
            Table priorityTable = priorityPatternQueryPlayer.excecute();
            time_end10k = System.currentTimeMillis();

            operationTree = init1kTree();
            PriorityDependenciesPatternQueryPlayer priorityDependenciespatternQueryPlayer = new PriorityDependenciesPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
            time_start100k = System.currentTimeMillis();
            Table priorityDTable = priorityDependenciespatternQueryPlayer.excecute();
            time_end100k = System.currentTimeMillis();

            operationTree = init1kTree();
            MultiLevelDependencyPlayer multiLevelDependencyPlayer = new MultiLevelDependencyPlayer(cubeTripleStoreQuery, operationTree);
            time_start1kk = System.currentTimeMillis();
            Table multiLevelTable = multiLevelDependencyPlayer.excecute();
            time_end1kk = System.currentTimeMillis();
            writer.write(String.valueOf(time_end1k - time_start1k) + "          "
                    + String.valueOf(time_end10k - time_start10k) + "          "
                    + String.valueOf(time_end100k - time_start100k) + "          "
                    + String.valueOf(time_end1kk - time_start1kk));
            writer.newLine();
        }
        writer.close();
    }

    @Test
    public void SubjectSearch10k() throws UnsupportedEncodingException, IOException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("10kLog.txt"), "utf-8"));
        writer.write("Secuent   Priority  Dependen  Multi");
        writer.newLine();
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(storage.getStore());
        CubeTripleStore cube = storage.getStore();
        long time_start1k, time_end1k;
        long time_start10k, time_end10k;
        long time_start100k, time_end100k;
        long time_start1kk, time_end1kk;
        OperationTree operationTree;

        for (int i = 0; i < 10000; i++) {
            operationTree = init10kTree();
            SecuentialPatternQueryPlayer sequientialPatternQueryPlayer = new SecuentialPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
            time_start1k = System.currentTimeMillis();
            Table sequientialTable = sequientialPatternQueryPlayer.excecute();
            time_end1k = System.currentTimeMillis();

            operationTree = init10kTree();
            PriorityPatternQueryPlayer priorityPatternQueryPlayer = new PriorityPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
            time_start10k = System.currentTimeMillis();
            Table priorityTable = priorityPatternQueryPlayer.excecute();
            time_end10k = System.currentTimeMillis();

            operationTree = init10kTree();
            PriorityDependenciesPatternQueryPlayer priorityDependenciespatternQueryPlayer = new PriorityDependenciesPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
            time_start100k = System.currentTimeMillis();
            Table priorityDTable = priorityDependenciespatternQueryPlayer.excecute();
            time_end100k = System.currentTimeMillis();

            operationTree = init10kTree();
            MultiLevelDependencyPlayer multiLevelDependencyPlayer = new MultiLevelDependencyPlayer(cubeTripleStoreQuery, operationTree);
            time_start1kk = System.currentTimeMillis();
            Table multiLevelTable = multiLevelDependencyPlayer.excecute();
            time_end1kk = System.currentTimeMillis();
            writer.write(String.valueOf(time_end1k - time_start1k) + "          "
                    + String.valueOf(time_end10k - time_start10k) + "          "
                    + String.valueOf(time_end100k - time_start100k) + "          "
                    + String.valueOf(time_end1kk - time_start1kk));
            writer.newLine();
        }
        writer.close();
    }

    @Test
    public void SubjectSearch100k() throws IOException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("100kLog.txt"), "utf-8"));
        writer.write("Secuent   Priority  Dependen  Multi");
        writer.newLine();
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(storage.getStore());
        CubeTripleStore cube = storage.getStore();
        long time_start1k, time_end1k;
        long time_start10k, time_end10k;
        long time_start100k, time_end100k;
        long time_start1kk, time_end1kk;
        OperationTree operationTree;

        for (int i = 0; i < 10000; i++) {
            operationTree = init100kTree();
            SecuentialPatternQueryPlayer sequientialPatternQueryPlayer = new SecuentialPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
            time_start1k = System.currentTimeMillis();
            Table sequientialTable = sequientialPatternQueryPlayer.excecute();
            time_end1k = System.currentTimeMillis();

            operationTree = init100kTree();
            PriorityPatternQueryPlayer priorityPatternQueryPlayer = new PriorityPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
            time_start10k = System.currentTimeMillis();
            Table priorityTable = priorityPatternQueryPlayer.excecute();
            time_end10k = System.currentTimeMillis();

            operationTree = init100kTree();
            PriorityDependenciesPatternQueryPlayer priorityDependenciespatternQueryPlayer = new PriorityDependenciesPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
            time_start100k = System.currentTimeMillis();
            Table priorityDTable = priorityDependenciespatternQueryPlayer.excecute();
            time_end100k = System.currentTimeMillis();

            operationTree = init100kTree();
            MultiLevelDependencyPlayer multiLevelDependencyPlayer = new MultiLevelDependencyPlayer(cubeTripleStoreQuery, operationTree);
            time_start1kk = System.currentTimeMillis();
            Table multiLevelTable = multiLevelDependencyPlayer.excecute();
            time_end1kk = System.currentTimeMillis();
            writer.write(String.valueOf(time_end1k - time_start1k) + "          "
                    + String.valueOf(time_end10k - time_start10k) + "          "
                    + String.valueOf(time_end100k - time_start100k) + "          "
                    + String.valueOf(time_end1kk - time_start1kk));
            writer.newLine();
        }
        writer.close();
    }

    @Test
    public void SubjectSearch1kk() throws IOException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("1kkLog.txt"), "utf-8"));
        writer.write("Secuent   Priority  Dependen  Multi");
        writer.newLine();
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(storage.getStore());
        CubeTripleStore cube = storage.getStore();
        long time_start1k, time_end1k;
        long time_start10k, time_end10k;
        long time_start100k, time_end100k;
        long time_start1kk, time_end1kk;
        OperationTree operationTree;

        for (int i = 0; i < 10000; i++) {
            operationTree = init1kkTree();
            SecuentialPatternQueryPlayer sequientialPatternQueryPlayer = new SecuentialPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
            time_start1k = System.currentTimeMillis();
            Table sequientialTable = sequientialPatternQueryPlayer.excecute();
            time_end1k = System.currentTimeMillis();

            operationTree = init1kkTree();
            PriorityPatternQueryPlayer priorityPatternQueryPlayer = new PriorityPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
            time_start10k = System.currentTimeMillis();
            Table priorityTable = priorityPatternQueryPlayer.excecute();
            time_end10k = System.currentTimeMillis();

            operationTree = init1kkTree();
            PriorityDependenciesPatternQueryPlayer priorityDependenciespatternQueryPlayer = new PriorityDependenciesPatternQueryPlayer(cubeTripleStoreQuery, operationTree);
            time_start100k = System.currentTimeMillis();
            Table priorityDTable = priorityDependenciespatternQueryPlayer.excecute();
            time_end100k = System.currentTimeMillis();

            operationTree = init1kkTree();
            MultiLevelDependencyPlayer multiLevelDependencyPlayer = new MultiLevelDependencyPlayer(cubeTripleStoreQuery, operationTree);
            time_start1kk = System.currentTimeMillis();
            Table multiLevelTable = multiLevelDependencyPlayer.excecute();
            time_end1kk = System.currentTimeMillis();
            writer.write(String.valueOf(time_end1k - time_start1k) + "          "
                    + String.valueOf(time_end10k - time_start10k) + "          "
                    + String.valueOf(time_end100k - time_start100k) + "          "
                    + String.valueOf(time_end1kk - time_start1kk));
            writer.newLine();
        }
        writer.close();
    }

    private OperationTree init1kTree() {
        OperationTree operationTree = new OperationTree();
        JoinNode joinNode = new JoinNode();
        joinNode.addNode(new PatternNode(initPattern(1)));
        operationTree.setRoot(joinNode);
        return operationTree;
    }

    private OperationTree init10kTree() {
        OperationTree operationTree = new OperationTree();
        JoinNode joinNode = new JoinNode();
        joinNode.addNode(new PatternNode(initPattern(4)));
        operationTree.setRoot(joinNode);
        return operationTree;
    }

    private OperationTree init100kTree() {
        OperationTree operationTree = new OperationTree();
        JoinNode joinNode = new JoinNode();
        joinNode.addNode(new PatternNode(initPattern(7)));
        operationTree.setRoot(joinNode);
        return operationTree;
    }

    private OperationTree init1kkTree() {
        OperationTree operationTree = new OperationTree();
        JoinNode joinNode = new JoinNode();
        joinNode.addNode(new PatternNode(initPattern(10)));
        operationTree.setRoot(joinNode);
        return operationTree;
    }

    private Pattern initPattern(int predicate) {
        return new Pattern(new What("Name"), new Constant(predicate), new What("foreing"));
    }
}
