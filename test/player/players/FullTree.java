package player.players;

import players.withDependencies.*;
import cubetriplestore.CubeTripleStoreQuery;
import dataPoolForTest.ForestOfTrueTrees;
import dataPoolForTest.StorageForTests;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import player.players.PriorityDependenciesPatternQueryPlayer;
import triskelTable.Table;
import triskeloperationtree.OperationTree;

public class FullTree extends StorageForTests {

    private final ForestOfTrueTrees trees;

    public FullTree() {
        this.trees = new ForestOfTrueTrees();
    }

    @Test
    public void twoJoinsTree() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());
        OperationTree operationTree = trees.twoJoins();
        PatternQueryPlayer patternQueryPlayer = new PatternQueryPlayer(cubeTripleStoreQuery, operationTree);
        Table table = patternQueryPlayer.excecute();
        assertEquals(1, table.getColumns().length);
        assertEquals(3, table.size());
    }

    @Test
    public void twoJoinsTreeBidimensional() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());
        OperationTree operationTree = trees.twoJoinsBidimensional();
        PatternQueryPlayer patternQueryPlayer = new PatternQueryPlayer(cubeTripleStoreQuery, operationTree);
        Table table = patternQueryPlayer.excecute();
        assertEquals(3, table.getColumns().length);
        assertEquals(5, table.size());
    }

    @Test
    public void authorQuery() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());
        OperationTree operationTree = trees.authorTreeQuery();
        PatternQueryPlayer patternQueryPlayer = new PatternQueryPlayer(cubeTripleStoreQuery, operationTree);
        Table table = patternQueryPlayer.excecute();
        assertEquals(3, table.getColumns().length);
        assertEquals(4, table.size());
        assertEquals("a", table.getColumns()[0]);
        assertEquals("title", table.getColumns()[1]);
        assertEquals("author", table.getColumns()[2]);
    }
}
