package player.players;

import players.withDependencies.*;
import cubetriplestore.CubeTripleStoreQuery;
import dataPoolForTest.ForestOfComplexTrees;
import dataPoolForTest.StorageForTests;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import triskelTable.Table;
import triskeloperationtree.OperationTree;

public class BidimensionalPattern extends StorageForTests {

    private final ForestOfComplexTrees trees;

    public BidimensionalPattern() {
        this.trees = new ForestOfComplexTrees();
    }

    @Test
    public void aWhatInSubjectPlace() {
        CubeTripleStoreQuery cubeTripleStoreQuery = new CubeTripleStoreQuery(initStore());
        OperationTree operationTree = trees.treeWithSubjectyObjectWhat();
        PatternQueryPlayer patternQueryPlayer = new PatternQueryPlayer(cubeTripleStoreQuery, operationTree);
        Table table = patternQueryPlayer.excecute();
        assertEquals(2, table.getColumns().length);
        assertEquals(10, table.size());
    }

}
